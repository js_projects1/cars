//returns an array of car_years
function get_car_years(inventory){
    let car_years=[]
    for (let i=0; i<inventory.length;i++){
        car_years.push(inventory[i].car_year)

    }
    return car_years
}
exports.get_car_years=get_car_years
