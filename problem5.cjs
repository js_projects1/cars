//returns an array of cars that are older than the year 2000
let _=require('./test/testProblem4.cjs')

function cars_older_than_year(inventory){
let car_years=_.car_years_list()
let old_cars=[]
    for (let i=0;i<car_years.length;i++){
        if (car_years[i]<2000){
            old_cars.push(inventory[i])
        }
    }
    console.log(old_cars)
    return old_cars;
}
exports.cars_older_than_year=cars_older_than_year