// gets the array of car models and returns sorted array by its alphabetical order
function sort_alphabetical(inventory){
    let car_models=[]
    for(let i=0; i<inventory.length;i++){
        car_models.push((inventory[i].car_model))
    }
    
    return car_models.sort();

}

exports.sort_alphabetical=sort_alphabetical